import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:untitled7/two.dart';
class One extends StatefulWidget {
  @override
  _OneState createState() => _OneState();
}
Widget _buildCircle({Color color, String photo , double height , double width , Color i_c , String text ,String text2 , BuildContext context}) {
  return
    InkWell(
        onTap: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (context)=>Screen2(photo,text)));
        },
        child:    Stack(
          children: <Widget>[


            Container(
              width: 200,
              height: 230,
            ),
            Positioned(
              right: 15,
              bottom: 50,
              child: Container(
                height: 200,
                width: 200,
                child: ClipRRect(
                    child: Image.asset("xx.png",fit: BoxFit.fill,color: color,)
                ),
              ),
            ),
            Positioned(
              right: 0,
              bottom: 50,
              child: Container(
                height: 100,
                width: 100,
                child: ClipRRect(
                    child: Image.asset("xxx.png",fit: BoxFit.fill,color: color,)
                ),
              ),
            ),
            Positioned(
              right: 48,
              bottom: 80,
              child: Container(
                height: height,
                width: width,
                child: ClipRRect(
                    child: Image.asset(photo)
                ),
              ),
            ),
            Positioned(
              right: 0,
              bottom: 50,
              child: Container(
                height: 100,
                width: 100,
                child: ClipRRect(
                    child: Icon (Icons.add , size: 35, color: i_c,)
                ),
              ),
            ),
            Positioned(
              right: 50,
              bottom: -40,
              child: Container(
                height: 100,
                width: 100,
                child: ClipRRect(
                    child: Text(text , style:TextStyle(fontSize: 25),)
                ),
              ),
            ),
            Positioned(
              right: 39,
              bottom: -70,
              child: Container(
                height: 100,
                width: 100,
                child: ClipRRect(
                    child: Text(text2 , style:TextStyle(fontSize: 22,fontWeight: FontWeight.bold),)
                ),
              ),
            ),
            Positioned(
              right: 13,
              bottom: -74,
              child: Container(
                height: 100,
                width: 100,
                child: ClipRRect(
                    child: Text("kg" , style:TextStyle(fontSize: 16),)
                ),
              ),
            ),


          ],
        )

    );

}
class _OneState extends State<One> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
     body: ListView(
          children: <Widget>[
            Row(children: <Widget>[
            Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
               child: Container(
                height: 65,
                width: 65,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  image: DecorationImage(image: AssetImage("1.jpg")),)
            )),
              SizedBox(
                width: 20,
              ),
              Text("Hi,Ayman",style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold),),
              SizedBox(
                width: 130,
              ),
              Icon(Icons.dehaze)
            ]
              ,),
            SizedBox(
              height: 20,
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text("What would you Like to eat?" ,style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                     child: Container(
                      width: MediaQuery.of(context).size.width-40,
                      height: 50,
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius:BorderRadius.circular(15),
                      ),

                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 13, vertical: 15),
                        child: Text("Search",style: TextStyle(fontSize: 18 ,color: Colors.grey),
                        ),

                      ),

                    ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

                _buildCircle(color: Color(0xffedf8b7) , photo : "A.png" , width : 135 , height: 135 ,i_c : Color(0xff89a224),text: "Apple",text2: '\$3'),
                _buildCircle(color: Color(0xfffeecec),photo : "B.png",width : 145 , height: 145,i_c : Color(0xffcc5055),text: "Lychee",text2: "\$4"),
            ],),
            SizedBox(
              height: 20,
            ),

            Row(mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _buildCircle(color: Color(0xffe3d8de),photo : "C.png",width : 160 , height: 160,i_c : Color(0xff673f50),text: "Figs",text2: "\$5"),
                _buildCircle(color: Color(0xffffe8d0),photo : "D.png",width : 130 , height: 130 ,i_c : Color(0xffd5954a),text: "Orange",text2: "\$6"),
              ],),

          ],
     )
    );
  }
}
