import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Screen2 extends StatefulWidget {
  final String text;
  final String photo;
  Screen2(this.text,this.photo);


  @override
  _Screen2State createState() => _Screen2State();
}

class _Screen2State extends State<Screen2> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor:Color(0xfffeecec),
    body: Container(
    width: MediaQuery.of(context).size.width,
    child: Column(
    children: <Widget>[
    Padding(
    padding: const EdgeInsets.all(15.0),
    child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
    Container(
    width: 50,
    height: 50,
    decoration: BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.all(Radius.circular(15))
    ),
    child: Center(
    child: Icon(Icons.arrow_back_ios,color: Colors.black,),
    ),
    ),
    Icon(Icons.more_vert,color: Colors.black,size: 30,)
    ],
    ),
    ),

    SizedBox(height: 10,),
    Container(
    width: 200,
    height: 200,
    child: Image.asset(widget.photo),
    ),
    SizedBox(height: 20,),
    Container(
    width: MediaQuery.of(context).size.width,
    height: 373,
    decoration: BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.only(
    topLeft: Radius.circular(25),
    topRight: Radius.circular(25)
    )
    ),
    child: Column(
    children: <Widget>[
    Padding(
    padding: EdgeInsets.all(10),
    ),
    Padding(
    padding: const EdgeInsets.only(right: 250),
    child: Text(widget.text,style: TextStyle(fontSize: 35,fontWeight: FontWeight.bold),),
    ),
    SizedBox(height: 15,),
    Padding(
    padding: const EdgeInsets.symmetric(horizontal: 20),
    child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
    Container(
    padding: EdgeInsets.symmetric(horizontal: 7),
    width: 120,
    height: 40,
    decoration: BoxDecoration(
    color: Colors.grey[200],
    borderRadius: BorderRadius.all(Radius.circular(10),
    ),
    ),
    child: Center(
    child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
    Icon(Icons.clear,color: Colors.grey,),
    Text("  2",style: TextStyle(fontSize: 18),),
    Text("Kg  ",style: TextStyle(fontSize: 15),),
    Container(
    width: 30,
    height: 30,
    decoration: BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.all(Radius.circular(10))),
    child: Icon(Icons.add,color: Colors.red,))
    ],
    ),
    ),
    ),
      Text('\$8.01',style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold),),
    ],
    ),
    ),
      SizedBox(height: 15),
      Padding(
        padding: const EdgeInsets.only(right: 265),
        child: Text("Description",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
      ),
      SizedBox(height: 10,),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Text('Native to southestern Asia,This golf ball-sized tropical fruit is looks similar to strawberry.it is a bumpy skin covers a translucent white flesh that is a good source of Vitamin C.',
          style: TextStyle(fontSize: 18,color: Colors.grey,letterSpacing: 1.3,),
        ),
      ),
      SizedBox(height: 20,),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Icon(Icons.star,color: Colors.red,size: 35,),
            Container(
              width: 270,
              height: 65,
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.all(Radius.circular(15)),
              ),
              child: Center(
                child: Text("Add to Card",style: TextStyle(
                    color: Colors.white,
                    fontSize: 22
                ),),
              ),
            )
          ],
        ),
      )
    ],
    ),
    ),
    ],
    ),
    ),
    );
  }
}